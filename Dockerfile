FROM archlinux:base-devel

RUN useradd --no-create-home --shell=/bin/false build && usermod -L build
RUN echo "build ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN echo "root ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

USER build
RUN sudo pacman -Sy --noconfirm git texlive-most inkscape emacs nodejs npm

# install yaspeller
RUN sudo npm install yaspeller -g

# install xits font
RUN git clone https://aur.archlinux.org/otf-xits.git /tmp/otf-xits
RUN cd /tmp/otf-xits && makepkg -si --noconfirm
